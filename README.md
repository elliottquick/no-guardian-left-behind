# No Guardian Left Behind #

This is a C#.NET (MVC4) based Website, originally hosted at: www.NoGuardianLeftBehind.com. It provided Players of Bungie's Destiny a place to find other like-minded players. At the time that it was developed Bungie didn't offer any streamlined Matchmaking services for the in-game activities that require the formation of an entire group. Like Raids and Nightfall strikes.

* Two sites are present, the In-Development (formerly) Umbraco website and the old custom MVC4 site

* Restful services (web api 2.2) site

* SignalR 2 was used for real-time updates related to the matchmaking search

* Backend was an Azure MSSQL server | the site was also hosted in Azure

* Bootstrap 3, used for Responsive design (prior to umbraco CMS)

* JQuery was used for most javascript calls (no angular.js or Vue.js made it past early development)

### What is this repository for? ###

* I have setup this Repo for anyone interested in a C# based MatchMaking system, using SignalR and the CMS Umbraco

* This is a Copy of the code origirinally developed in my TFS location: https://destinygroupfinder.visualstudio.com/No%20Guardian%20Left%20Behind/_versionControl  

* Version 1.0
